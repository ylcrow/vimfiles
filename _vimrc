" =============================================================================
"                          << GUI OP_SYSTEM配置 >>
" =============================================================================
if(has("win32") || has("win64") || has("win95") || has("win16"))
    let g:iswindows = 1
else
    let g:iswindows = 0
    if filereadable("/System/Library/Kernels/kernel")
        let g:isMac = 1
    else
        let g:isMac = 0
    endif
endif

if has("gui_running")
    let g:isGUI = 1
else
    let g:isGUI = 0
endif


if (g:iswindows && g:isGUI)
    source $VIMRUNTIME/vimrc_example.vim
    source $VIMRUNTIME/mswin.vim
    behave mswin
    set diffexpr=MyDiff()

    function MyDiff()
        let opt = '-a --binary '
        if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
        if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
        let arg1 = v:fname_in
        if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
        let arg2 = v:fname_new
        if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
        let arg3 = v:fname_out
        if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
        let eq = ''
        if $VIMRUNTIME =~ ' '
            if &sh =~ '\<cmd'
                let cmd = '""' . $VIMRUNTIME . '\diff"'
                let eq = '"'
            else
                let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
            endif
        else
            let cmd = $VIMRUNTIME . '\diff'
        endif
        silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
    endfunction

endif


let mapleader=","									  "map leader

if !g:iswindows
    set hlsearch        "高亮搜索
    set incsearch       "在输入要搜索的文字时，实时匹配

    " Uncomment the following to have Vim jump to the last position when
    " reopening a file
    if has("autocmd")
        au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
    endif

    if g:isGUI
        " Source a global configuration file if available
        if filereadable("/etc/vim/gvimrc.local")
            source /etc/vim/gvimrc.local
        endif
    else
        " This line should not be removed as it ensures that various options are
        " properly set to work with the Vim-related packages available in Debian.
        runtime! debian.vim

        " Vim5 and later versions support syntax highlighting. Uncommenting the next
        " line enables syntax highlighting by default.
        if has("syntax")
            syntax on
        endif

        set mouse=a                    " 在任何模式下启用鼠标
        set t_Co=256                   " 在终端启用256色
        set backspace=2                " 设置退格键可用

        " Source a global configuration file if available
        if filereadable("/etc/vim/vimrc.local")
            source /etc/vim/vimrc.local
        endif
    endif
	"快速编辑vimrc文件
	map <silent> <leader>ss :source ~/.vimrc<cr>
	map <silent> <leader>ee :e ~/.vimrc<cr>
	autocmd! bufwritepost .vimrc source ~/.vimrc
else
	"快速编辑vimrc文件
	"map <silent> <leader>ss :source $VIM\_vimrc<cr> "报告函数重复 待修复 
	map <silent> <leader>ee :e $VIM\_vimrc<cr>
	"autocmd! bufwritepost _vimrc source $VIM\_vimrc
endif


if g:isGUI
	"winpos 100 10                                         "指定窗口出现的位置，坐标原点在屏幕左上角
	"set lines=38 columns=120                              "指定窗口大小，lines为高度，columns为宽度
	au GUIEnter * simalt ~x                           "窗口启动时自动最大化
    set guioptions-=m 								  "显示/隐藏菜单栏、工具栏、滚动条，可用 Ctrl + F11 切换
    set guioptions-=T
    set guioptions-=r
    set guioptions-=L
    map <silent> <c-F11> :if &guioptions =~# 'm' <Bar>
        \set guioptions-=m <Bar>
        \set guioptions-=T <Bar>
        \set guioptions-=r <Bar>
        \set guioptions-=L <Bar>
    \else <Bar>
        \set guioptions+=m <Bar>
        \set guioptions+=T <Bar>
        \set guioptions+=r <Bar>
        \set guioptions+=L <Bar>
    \endif<CR>
    colorscheme desert_my 							  "GUI配色方案
else
    colorscheme desert_my "终端配色方案
    "colorscheme default_my "终端配色方案
endif


"function Session Manage
let g:cupath = getcwd()
function! MakeSession()
	let sessionopt_cmd1 = "set sessionoptions-=curdir"
	let sessionopt_cmd2 = "set sessionoptions+=sesdir"
	let sessionopt_cmd3 = "set sessionoptions+=unix"
	let sessionopt_cmd4 = "set sessionoptions+=slash"
	let session_cmd = "mksession! " . g:cupath . "/workspace.vim"
	let info_cmd = "wviminfo! " . g:cupath . "/workspace.viminfo"
	exe sessionopt_cmd1 
	exe sessionopt_cmd2 
	exe sessionopt_cmd3 
	exe sessionopt_cmd4 
	exe session_cmd 
	exe info_cmd 
	x
	x
	x
	x
endfunction
"execution sequence
" 	1.workspace.vim;
" 	2.workspacex.vim;  attach [**x.vim]
" 		ex:	set path+=.,/usr/include,,,~/**
"			. pwd
"			, cur work dir
"			path for cmd [ctrl+w + f]
"			path for [gf]
function! ReadSession()
	if !filereadable("workspace.vim")
		echo "workspace.vim is not exist!"
		return
	endif
	let session_cmd = "source " . g:cupath . "/workspace.vim"
	let info_cmd = "rviminfo " . g:cupath . "/workspace.viminfo"
	exe session_cmd
	exe info_cmd
endfunction
noremap <F7> :call MakeSession()<CR>
noremap <F8> :call ReadSession()<CR><CR>


augroup Binary
  au!
  au BufReadPre   *.bin,*.o,*.exe let &bin=1
  au BufReadPost  *.bin,*.o,*.exe if &bin | %!xxd
  au BufReadPost  *.bin,*.o,*.exe set ft=xxd | endif
  au BufWritePre  *.bin,*.o,*.exe if &bin | %!xxd -r
  au BufWritePre  *.bin,*.o,*.exe endif
  au BufWritePost *.bin,*.o,*.exe if &bin | %!xxd
  au BufWritePost *.bin,*.o,*.exe set nomod | endif
augroup END



set number                                            "显示行号
if g:isGUI
	set cursorline                                        "突出显示当前行
endif
set laststatus=2                                      "启用状态栏信息
"set nowrap                                            "设置不自动换行
set encoding=utf-8                                    "文件读入内存后会转换为enc编码，应该与locale对应
set fileencoding=utf-8                                "对应当前文件保存在磁盘的编码,这里的设置只对应新文件的保持编码
set fileencodings=ucs-bom,utf-8,gbk,cp936,latin-1     "设置支持打开的文件的编码
"set termencoding=utf-8
if (g:iswindows && g:isGUI)
	 "解决菜单乱码
    source $VIMRUNTIME/delmenu.vim
    source $VIMRUNTIME/menu.vim

    "解决consle输出乱码
    language messages zh_CN.utf-8
endif

set fileformat=unix                                   "设置新文件的<EOL>格式
set fileformats=unix,dos,mac                          "给出文件的<EOL>格式类型
filetype on                                           "启用文件类型侦测
filetype plugin on                                    "针对不同的文件类型加载对应的插件
filetype plugin indent on                             "启用缩进
set writebackup                            			  "保存文件前建立备份，保存成功后删除该备份
set nobackup                                 		  "设置无备份文件,(~结尾的文件)
"set noswapfile                             		  "设置无临时文件
set vb t_vb=                                 		  "关闭提示音
set smartindent                                       "启用智能对齐方式
set tabstop=4                                         "设置Tab键的宽度
set expandtab                                         "Use space when <TAB> is inserted
set cindent shiftwidth=4                              "C语言换行时自动缩进
set smarttab                                          "指定按一次backspace就删除shiftwidth宽度的空格
set foldenable                                        "启用折叠
set foldmethod=indent                                 "indent 折叠方式
set foldlevel=100									  "自动折叠level, 越大展开的越多
set ignorecase                                        "搜索模式里忽略大小写
set smartcase                                         "如果搜索模式包含大写字符，不使用 'ignorecase' 选项，只有在输入搜索模式并且打开 'ignorecase' 选项时才会使用
set noincsearch                                       "在输入要搜索的文字时，取消实时匹配
set autoread 										  "当文件在外部被修改，自动更新该文件
set novisualbell									  "取消闪烁警告

" 个性化状栏（这里提供两种方式，要使用其中一种去掉注释即可，不使用反之）
let &statusline=' %t %{&mod?(&ro?"*":"+"):(&ro?"=":" ")} %1*|%* %{&ft==""?"any":&ft} %1*|%* %{&ff} %1*|%* %{(&fenc=="")?&enc:&fenc}%{(&bomb?",BOM":"")} %1*|%* %=%1*|%* 0x%B %1*|%* (%l,%c%V) %1*|%* %L %1*|%* %P'
"set statusline=%t\ %1*%m%*\ %1*%r%*\ %2*%h%*%w%=%l%3*/%L(%p%%)%*,%c%V]\ [%b:0x%B]\ [%{&ft==''?'TEXT':toupper(&ft)},%{toupper(&ff)},%{toupper(&fenc!=''?&fenc:&enc)}%{&bomb?',BOM':''}%{&eol?'':',NOEOL'}]

"set expandtab                                         "将Tab键转换为空格
"set foldmethod=marker                                 "marker 折叠方式
"set foldmethod=manual                                 "manual 折叠方式
"set foldmarker={,}                                    "manual 折叠方式
"set guifont=YaHei_Consolas_Hybrid:h10                 "设置字体:字号（字体名称空格用下划线代替）
"set shortmess=atI                                     "去掉欢迎界面

"用空格键来开关折叠
nnoremap <space> @=((foldclosed(line('.')) < 0) ? 'zc' : 'zo')<CR>   

" 常规模式下输入 cS 清除行尾空格
nmap cS :%s/\s\+$//g<cr>:noh<cr>

" 常规模式下输入 cM 清除行尾 ^M 符号
nmap cM :%s/\r$//g<cr>:noh<cr>

" 插入模式使用jj替换esc
imap jj <Esc>

" Ctrl + K 插入模式下光标向上移动
imap <c-k> <Up>

" Ctrl + J 插入模式下光标向下移动
imap <c-j> <Down>

" Ctrl + H 插入模式下光标向左移动
imap <c-h> <Left>

" Ctrl + L 插入模式下光标向右移动
imap <c-l> <Right>

" 每行超过80个的字符用下划线标示
"au BufWinEnter * let w:m2=matchadd('Underlined', '\%>' . 80 . 'v.\+', -1)

" 在不使用 MiniBufExplorer 插件时也可用<C-k,j,h,l>切换到上下左右的窗口中去
noremap <c-k> <c-w>k
noremap <c-j> <c-w>j
noremap <c-h> <c-w>h
noremap <c-l> <c-w>l

" for 快捷输入 类似 snipMate
autocmd BufEnter *.c 	abbr FOR for (i = 0; i < 3; i++) {<CR>}<Esc>0
" autocmd BufLeave *.c 	unabbr FOR

" 自动切换目录为当前编辑文件所在目录
"au BufRead,BufNewFile,BufEnter * cd %:p:h

"当前文件中快速查找光标下的单词,放入quickfix
nmap <leader>grep :lv /<c-r>=expand("<cword>")<cr>/j  % **/*.*<cr>:lw<cr>





























" -----------------------------------------------------------------------------
"  < 编译、连接、运行配置 >
" -----------------------------------------------------------------------------
" F9 一键保存并编译
map <F9> :call Compile()<CR>
imap <F9> <ESC>:call Compile()<CR>

" F10 一键保存并连接
map <F10> :call Link()<CR>
imap <F10> <ESC>:call Link()<CR>

" F11 一键保存、编译、连接存并运行
map <F11> :call Run()<CR>
imap <F11> <ESC>:call Run()<CR>

let s:LastShellReturn_C = 0
let s:LastShellReturn_L = 0
let s:ShowWarning = 1
let s:Obj_Extension = '.o'
let s:Exe_Extension = '.exe'
let s:Sou_Error = 0

let s:windows_CFlags = 'gcc\ -fexec-charset=gbk\ -Wall\ -g\ -O0\ -c\ %\ -o\ %<.o'
let s:linux_CFlags = 'gcc\ -Wall\ -g\ -O0\ -c\ %\ -o\ %<.o'

let s:windows_CPPFlags = 'g++\ -fexec-charset=gbk\ -Wall\ -g\ -O0\ -c\ %\ -o\ %<.o'
let s:linux_CPPFlags = 'g++\ -Wall\ -g\ -O0\ -c\ %\ -o\ %<.o'

func! Compile()
    exe ":ccl"
    exe ":update"
    if expand("%:e") == "c" || expand("%:e") == "cpp" || expand("%:e") == "cxx"
        let s:Sou_Error = 0
        let s:LastShellReturn_C = 0
        let Sou = expand("%:p")
        let Obj = expand("%:p:r").s:Obj_Extension
        let Obj_Name = expand("%:p:t:r").s:Obj_Extension
        let v:statusmsg = ''
        if !filereadable(Obj) || (filereadable(Obj) && (getftime(Obj) < getftime(Sou)))
            redraw!
            if expand("%:e") == "c"
                if g:iswindows
                    exe ":setlocal makeprg=".s:windows_CFlags
                else
                    exe ":setlocal makeprg=".s:linux_CFlags
                endif
                echohl WarningMsg | echo " compiling..."
                silent make
            elseif expand("%:e") == "cpp" || expand("%:e") == "cxx"
                if g:iswindows
                    exe ":setlocal makeprg=".s:windows_CPPFlags
                else
                    exe ":setlocal makeprg=".s:linux_CPPFlags
                endif
                echohl WarningMsg | echo " compiling..."
                silent make
            endif
            redraw!
            if v:shell_error != 0
                let s:LastShellReturn_C = v:shell_error
            endif
            if g:iswindows
                if s:LastShellReturn_C != 0
                    exe ":bo cope"
                    echohl WarningMsg | echo " compilation failed"
                else
                    if s:ShowWarning
                        exe ":bo cw"
                    endif
                    echohl WarningMsg | echo " compilation successful"
                endif
            else
                if empty(v:statusmsg)
                    echohl WarningMsg | echo " compilation successful"
                else
                    exe ":bo cope"
                endif
            endif
        else
            echohl WarningMsg | echo ""Obj_Name"is up to date"
        endif
    else
        let s:Sou_Error = 1
        echohl WarningMsg | echo " please choose the correct source file"
    endif
    exe ":setlocal makeprg=make"
endfunc

func! Link()
    call Compile()
    if s:Sou_Error || s:LastShellReturn_C != 0
        return
    endif
    let s:LastShellReturn_L = 0
    let Sou = expand("%:p")
    let Obj = expand("%:p:r").s:Obj_Extension
    if g:iswindows
        let Exe = expand("%:p:r").s:Exe_Extension
        let Exe_Name = expand("%:p:t:r").s:Exe_Extension
    else
        let Exe = expand("%:p:r")
        let Exe_Name = expand("%:p:t:r")
    endif
    let v:statusmsg = ''
	if filereadable(Obj) && (getftime(Obj) >= getftime(Sou))
        redraw!
        if !executable(Exe) || (executable(Exe) && getftime(Exe) < getftime(Obj))
            if expand("%:e") == "c"
                setlocal makeprg=gcc\ -o\ %<\ %<.o
                echohl WarningMsg | echo " linking..."
                silent make
            elseif expand("%:e") == "cpp" || expand("%:e") == "cxx"
                setlocal makeprg=g++\ -o\ %<\ %<.o
                echohl WarningMsg | echo " linking..."
                silent make
            endif
            redraw!
            if v:shell_error != 0
                let s:LastShellReturn_L = v:shell_error
            endif
            if g:iswindows
                if s:LastShellReturn_L != 0
                    exe ":bo cope"
                    echohl WarningMsg | echo " linking failed"
                else
                    if s:ShowWarning
                        exe ":bo cw"
                    endif
                    echohl WarningMsg | echo " linking successful"
                endif
            else
                if empty(v:statusmsg)
                    echohl WarningMsg | echo " linking successful"
                else
                    exe ":bo cope"
                endif
            endif
        else
            echohl WarningMsg | echo ""Exe_Name"is up to date"
        endif
    endif
    setlocal makeprg=make
endfunc

func! Run()
    let s:ShowWarning = 0
    call Link()
    let s:ShowWarning = 1
    if s:Sou_Error || s:LastShellReturn_C != 0 || s:LastShellReturn_L != 0
        return
    endif
    let Sou = expand("%:p")
    let Obj = expand("%:p:r").s:Obj_Extension
    if g:iswindows
        let Exe = expand("%:p:r").s:Exe_Extension
    else
        let Exe = expand("%:p:r")
    endif
    if executable(Exe) && getftime(Exe) >= getftime(Obj) && getftime(Obj) >= getftime(Sou)
        redraw!
        echohl WarningMsg | echo " running..."
        if g:iswindows
            exe ":!%<.exe"
        else
            if g:isGUI
                exe ":!gnome-terminal -e ./%<"
            else
                exe ":!./%<"
            endif
        endif
        redraw!
        echohl WarningMsg | echo " running finish"
    endif
endfunc



























" =============================================================================
"                          << 以下为常用插件配置 >>
" =============================================================================


" -----------------------------------------------------------------------------
"  < go lang插件管理 >
" ----------------------------------------------------------------------------- 
filetype off
filetype plugin indent off
set runtimepath+=$GOROOT/misc/vim
filetype plugin indent on
syntax on



" -----------------------------------------------------------------------------
"  < Vundle 插件管理工具配置 >
" -----------------------------------------------------------------------------
set nocompatible                                      "禁用 Vi 兼容模式
filetype off                                          "禁用文件类型侦测

if !g:iswindows
    set rtp+=~/.vim/bundle/vundle/
    call vundle#rc()
else
    set rtp+=$VIM/vimfiles/bundle/vundle/
    call vundle#rc('$VIM/vimfiles/bundle/')
endif

Bundle 'gmarik/vundle'
"Bundle 'Align'
if g:isGUI
	Bundle 'oblitum/cSyntaxAfter'
	Bundle 'Yggdroot/indentLine'
	Bundle 'std_c.zip'
endif
Bundle 'Lokaltog/vim-powerline'
Bundle 'jiangmiao/auto-pairs'
Bundle 'bufexplorer.zip'
Bundle 'ccvext.vim'
Bundle 'Shougo/neocomplcache'
Bundle 'scrooloose/nerdcommenter'
Bundle 'scrooloose/nerdtree'
Bundle 'wesleyche/SrcExpl'
Bundle 'majutsushi/tagbar'
Bundle 'netroby/taglist'
Bundle 'TxtBrowser'
Bundle 'ZoomWin'
Bundle 'DrawIt'

filetype plugin indent on                             "启用缩进


" -----------------------------------------------------------------------------
"  < Align 插件配置 >
" -----------------------------------------------------------------------------
" 一个对齐的插件，用来——排版与对齐代码，功能强大，不过用到的机会不多


" -----------------------------------------------------------------------------
"  < auto-pairs 插件配置 >
" -----------------------------------------------------------------------------
" 用于括号与引号自动补全，不过会与函数原型提示插件echofunc冲突


" -----------------------------------------------------------------------------
"  < BufExplorer 插件配置 >
" -----------------------------------------------------------------------------
" 快速轻松的在缓存中切换（相当于另一种多个文件间的切换方式）
" <Leader>be 在当前窗口显示缓存列表并打开选定文件
" <Leader>bs 水平分割窗口显示缓存列表，并在缓存列表窗口中打开选定文件
" <Leader>bv 垂直分割窗口显示缓存列表，并在缓存列表窗口中打开选定文件



" -----------------------------------------------------------------------------
"  < ccvext.vim 插件配置 >
" -----------------------------------------------------------------------------
" 用于对指定文件自动生成tags与cscope文件并连接
" 如果是Windows系统, 则生成的文件在源文件所在盘符根目录的.symbs目录下(如: X:\.symbs\)
" 如果是Linux系统, 则生成的文件在~/.symbs/目录下
" 具体用法可参考www.vim.org中此插件的说明
" <Leader>sy 自动生成tags与cscope文件并连接
" <Leader>sc 连接已存在的tags与cscope文件



" -----------------------------------------------------------------------------
"  < cSyntaxAfter 插件配置 >
" -----------------------------------------------------------------------------
" 高亮括号与运算符等
if g:isGUI
	au! BufRead,BufNewFile,BufEnter *.{c,cpp,h,javascript} call CSyntaxAfter()
endif


" -----------------------------------------------------------------------------
"  < indentLine 插件配置 > vim7.3支持
" -----------------------------------------------------------------------------
" 用于显示对齐线，与 indent_guides 在显示方式上不同，根据自己喜好选择了
" 在终端上会有屏幕刷新的问题，这个问题能解决有更好了
" 开启/关闭对齐线
" 设置Gvim的对齐线样式
" 设置终端对齐线颜色
" let g:indentLine_color_term = 239
" 设置 GUI 对齐线颜色
" let g:indentLine_color_gui = '#A4E57E'
if g:isGUI
	nmap <leader>il :IndentLinesToggle<CR>
    let g:indentLine_char = "┊"
    let g:indentLine_first_char = "┊"
endif



" -----------------------------------------------------------------------------
"  < std_c 插件配置 >
" -----------------------------------------------------------------------------
" 用于增强C语法高亮
" 启用 // 注视风格
if g:isGUI
	let c_cpp_comments = 0
endif



" -----------------------------------------------------------------------------
"  < neocomplcache 插件配置 >
" -----------------------------------------------------------------------------
" 关键字补全、文件路径补全、tag补全等等，各种，非常好用，速度超快。
let g:neocomplcache_enable_at_startup = 1     "vim 启动时启用插件
" let g:neocomplcache_disable_auto_complete = 1 "不自动弹出补全列表
" 在弹出补全列表后用 <c-p> 或 <c-n> 进行上下选择效果比较好





" -----------------------------------------------------------------------------
"  < nerdcommenter 插件配置 >
" -----------------------------------------------------------------------------
" 我主要用于C/C++代码注释(其它的也行)，这个插件我做了小点修改，也就是在注释符
" 与注释内容间加一个空格
" 以下为插件默认快捷键，其中的说明是以C/C++为例的
" <Leader>cm 以一个 /* */ 注释选中行(选中区域所在行)，再输入则称重复注释
" <Leader>cu 取消选中区域(行)的注释，选中区域(行)内至少有一个 /* */
" <Leader>ci 以每行一个 /* */ 注释选中行(选中区域所在行)，再输入则取消注释
" <Leader>cc 以每行一个 /* */ 注释选中行或区域，再输入则称重复注释
" <Leader>ca 在/*...*/与//这两种注释方式中切换（其它语言可能不一样了）
" <Leader>cA 行尾注释
let NERDSpaceDelims = 1                     "在左注释符之后，右注释符之前留有空格




" -----------------------------------------------------------------------------
"  < nerdtree 插件配置 >
" -----------------------------------------------------------------------------
" 有目录村结构的文件浏览插件
" 常规模式下输入 F5 调用插件
nmap <F5> :NERDTreeToggle<CR>




" -----------------------------------------------------------------------------
"  < powerline 插件配置 >
" -----------------------------------------------------------------------------
" 状态栏插件，更好的状态栏效果




" -----------------------------------------------------------------------------
"  < SrcExpl 插件配置 >
" -----------------------------------------------------------------------------
" 增强源代码浏览，其功能就像Windows中的"Source Insight"
" :SrcExpl                                   "打开浏览窗口
" :SrcExplClose                              "关闭浏览窗口
" :SrcExplToggle                             "打开/闭浏览窗口
let g:SrcExpl_winHeight = 8
let g:SrcExpl_refreshTime = 100
let g:SrcExpl_isUpdateTags = 0
let g:SrcExpl_updataTagsKey = "<c-F4>"
noremap <F4> :SrcExplToggle<CR>



" -----------------------------------------------------------------------------
"  < Tagbar 插件配置 >
" -----------------------------------------------------------------------------
" 相对 TagList 能更好的支持面向对象

" 常规模式下输入 tb 调用插件，如果有打开 TagList 窗口则先将其关闭
nmap tb :TlistClose<cr>:TagbarToggle<cr>

let g:tagbar_width=30                       "设置窗口宽度
" let g:tagbar_left=1                         "在左侧窗口中显示





" -----------------------------------------------------------------------------
"  < TagList 插件配置 >
" -----------------------------------------------------------------------------
" 常规模式下输入 tl 调用插件，如果有打开 Tagbar 窗口则先将其关闭
nmap tl :TagbarClose<cr>:Tlist<cr>
if g:isMac
    let Tlist_Ctags_Cmd='/usr/local/bin/ctags'
endif
let Tlist_Show_One_File=1                   "只显示当前文件的tags
let Tlist_Enable_Fold_Column=0              "使taglist插件不显示左边的折叠行
let Tlist_Exit_OnlyWindow=1                 "如果Taglist窗口是最后一个窗口则退出Vim
let Tlist_File_Fold_Auto_Close=0            "自动折叠
let Tlist_WinWidth=30                       "设置窗口宽度
let Tlist_Use_Right_Window=1                "在右侧窗口中显示
"let g:Tlist_Show_Menu = 1




" -----------------------------------------------------------------------------
"  < txtbrowser 插件配置 >
" -----------------------------------------------------------------------------
" 用于文本文件生成标签与与语法高亮（调用TagList插件生成标签，如果可以）
au BufRead,BufNewFile *.txt setlocal ft=txt




" -----------------------------------------------------------------------------
"  < ZoomWin 插件配置 >
" -----------------------------------------------------------------------------
" 用于分割窗口的最大化与还原
" 快捷键 <c-w>o 在最大化与还原间切换





" -----------------------------------------------------------------------------
"  < cscope 工具配置 >
" -----------------------------------------------------------------------------
if has("cscope")
    "设定可以使用 quickfix 窗口来查看 cscope 结果
    set cscopequickfix=s-,c-,d-,i-,t-,e-
    "使支持用 Ctrl+]  和 Ctrl+t 快捷键在代码间跳转
    set cscopetag
    "如果你想反向搜索顺序设置为1
    set csto=0
    "在当前目录中添加任何数据库
    if filereadable("cscope.out")
        cs add cscope.out
    "否则添加数据库环境中所指出的
    elseif $CSCOPE_DB != ""
        cs add $CSCOPE_DB
    endif
    set cscopeverbose
    "快捷键设置
    nmap <C-\>s :cs find s <C-R>=expand("<cword>")<CR><CR>
    nmap <leader>def :cs find g <C-R>=expand("<cword>")<CR><CR>
    nmap <leader>cal :cs find c <C-R>=expand("<cword>")<CR><CR>:cw<CR>
    nmap <C-\>t :cs find t <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>e :cs find e <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
    nmap <C-\>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
    nmap <C-\>d :cs find d <C-R>=expand("<cword>")<CR><CR>
endif
"s:Find this C symbol
"g:Find this global definition
"d:Find functions called by this func
"c:Find functions calling this function
"t:Find this text string
"e:this egrep pattern
"f:Find this file
"i:Find files #including this file






" -----------------------------------------------------------------------------
"  < ctags 工具配置 >
" -----------------------------------------------------------------------------
" 对浏览代码非常的方便,可以在函数,变量之间跳转等
"set tags=./tags





" -----------------------------------------------------------------------------
"  < gvimfullscreen 工具配置 > 请确保已安装了工具
" -----------------------------------------------------------------------------
" 用于 Windows Gvim 全屏窗口，可用 ctrl-F10 切换
" 全屏后再隐藏菜单栏、工具栏、滚动条效果更好
if (g:iswindows && g:isGUI)
    map <c-F10> <Esc>:call libcallnr("gvimfullscreen.dll", "ToggleFullScreen", 0)<CR>
endif




" -----------------------------------------------------------------------------
"  < vimtweak 工具配置 > 请确保以已装了工具
" -----------------------------------------------------------------------------
" 这里只用于窗口透明与置顶
" 常规模式下 Shift + k 减小透明度，Shift + j 增加透明度，ctrl + f9窗口置顶与否切换
if (g:iswindows && g:isGUI)
    let g:Current_Alpha = 230
    let g:Top_Most = 0
    func! Alpha_add()
        let g:Current_Alpha = g:Current_Alpha + 10
        if g:Current_Alpha > 255
            let g:Current_Alpha = 255
        endif
        call libcallnr("vimtweak.dll","SetAlpha",g:Current_Alpha)
    endfunc
    func! Alpha_sub()
        let g:Current_Alpha = g:Current_Alpha - 10
        if g:Current_Alpha < 155
            let g:Current_Alpha = 155
        endif
        call libcallnr("vimtweak.dll","SetAlpha",g:Current_Alpha)
    endfunc
    func! Top_window()
        if  g:Top_Most == 0
            call libcallnr("vimtweak.dll","EnableTopMost",1)
            let g:Top_Most = 1
        else
            call libcallnr("vimtweak.dll","EnableTopMost",0)
            let g:Top_Most = 0
        endif
    endfunc
	autocmd GUIEnter * call libcallnr("vimtweak.dll", "SetAlpha",g:Current_Alpha)
    "快捷键设置
    map <s-k> :call Alpha_add()<cr>
    map <s-j> :call Alpha_sub()<cr>
    map <c-F9> :call Top_window()<cr>
endif







" -----------------------------------------------------------------------------
"  <文件模板配置> 
" -----------------------------------------------------------------------------
let g:template_load = 1
let g:template_tags_replacing = 1
let g:T_AUTHOR = "XianWang"
"let g:T_AUTHOR_EMAIL = "xianwang1989@gmail.com"
"let g:T_DATE_FORMAT = "%c"
let g:T_DATE_FORMAT = "%m-%d-%Y %H:%M:%S"
"let g:T_LICENSE = ""
let g:T_PROJECT = "NewArk"
let g:T_COMPANY = "Copyright (c) 2014 NewArk,Co.,Ltd."



