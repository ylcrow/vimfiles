/**************************************************************************
**  <T_COMPANY>
**
**  Project: <T_PROJECT>
**  File:    <T_FILENAME>
**  Author:  <T_AUTHOR> <<T_AUTHOR_EMAIL>>
**  Date:    <T_CREATE_DATE> 
**
**  Purpose:
**************************************************************************/


/* Include files. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>

#include <sys/errno.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>


/* Macro constant definitions. */


/* Type definitions. */


/* Local function declarations. */


/* Macro API definitions. */


/* Global variable declarations. */


/* Extern function realize. */


/* Local function realize. */



