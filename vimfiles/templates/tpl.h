/**************************************************************************
**  <T_COMPANY>
**
**  Project: <T_PROJECT>
**  File:    <T_FILENAME>
**  Author:  <T_AUTHOR> <<T_AUTHOR_EMAIL>>
**  Date:    <T_CREATE_DATE> 
**
**  Purpose:
**************************************************************************/
#ifndef <T_HEAD> 
#define <T_HEAD>


#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

/* Include files. */


/* Macro constant definitions. */


/* Type definitions. */


/* External function declarations. */


/* Macro API definitions. */


/* Global variable declarations. */


#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif /* <T_HEAD> */


