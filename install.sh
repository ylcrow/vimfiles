#!/bin/bash
platform=`uname`
dir=`pwd`
if [ "$platform" == "Darwin" ]
then
    rm ~/.vimrc; ln -s  $dir/_vimrc     ~/.vimrc
    rm ~/.vim;   ln -s  $dir/vimfiles   ~/.vim
else
    echo 'No!!'
fi

